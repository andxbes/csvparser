/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cvscalc;

import com.csvreader.CsvReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Андрей
 */
public class CvsCalc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
	Map<String, Long> map = new HashMap<>();
	try {

	    CsvReader products = new CsvReader("./1.csv", ',', Charset.forName("utf-8"));

	    products.readHeaders();

	    while (products.readRecord()) {

		String name = products.get(1);//название проекта 
		Long time = null;
		try {
		    time = get_minutes(products.get(6));//время
		    System.out.println(name + " " + time);

		    if (map.containsKey(name)) {
			Long t = map.get(name);
			t += time;
			map.put(name, t);

		    } else {

			map.put(name, time);
		    }
		} catch (NumberFormatException e) {
		    System.out.println(e);
		    continue;
		}

	    }
	    products.close();

	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	System.out.println("============================ Результаты ===========================");
	for (Map.Entry<String, Long> entrySet : map.entrySet()) {
	    String key = entrySet.getKey();
	    Long value = entrySet.getValue();

	    System.out.println(key + " = " + get_hours_and_minutes(value));
	}

    }

    public static String get_hours_and_minutes(long l) {

	int hours = (int) (l / 60);
	int minutes = (int) l - (hours * 60);

	return hours + ":" + (minutes < 10 ? "0" + minutes : minutes);
    }

    public static long get_minutes(String s) {
	String arr[] = s.split(":");
	int hours = Integer.parseInt(arr[0]);

	int minutes = Integer.parseInt(arr[1]);
	return ((minutes) + (hours * 60));
    }

}
